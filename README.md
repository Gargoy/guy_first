# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A class for a simple server - version 1.0

### Dependencies ###
The following headers are included for the "Guy_Socket" class to work:
<winsock2.h>
<windows.h>
<iostream>
<string>

### How to use? ###
Simply create an instance of Guy_Socket class:

Guy_Socket *server = new Guy_Socket(PORT,TEXT_TO_SEND)

PORT - the server port.
TEXT_TO_SEND - the text to send to a connected client.

