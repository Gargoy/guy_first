#include "Guy_Socket.h"


 Guy_Socket::Guy_Socket(int port,string data)
{
	WSADATA info;
	int err;
	SOCKET listenSocket = initSocket(), acceptSocket;
	sockaddr_in server = initServer(port);
	err = WSAStartup(MAKEWORD(2, 0), &info);
	if (err != 0)
	{
		cout << "WSAStartup failed code %d\n" << err;;
	}
	while (1)
	{
		bind_sock(listenSocket, (sockaddr *)&server, sizeof(server));

		listening(listenSocket);

		cout << "Waiting for client to connect...\n";

		acceptSocket = accept(listenSocket, NULL, NULL);

		if (acceptSocket == INVALID_SOCKET)
		{
			cout << "accept function failed with error: %ld\n" << WSAGetLastError();

			acceptSocket = accept(listenSocket, NULL, NULL);
		}

		cout << "Client connected.\n";

		recive(acceptSocket, nullptr, 11, 0);


		send_data(acceptSocket, data.c_str(), data.length(), 0);
	}
	closesocket(acceptSocket);
	closesocket(listenSocket);
}




 //initiate a socket
SOCKET Guy_Socket::initSocket()
{
	SOCKET s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s == INVALID_SOCKET)
	{
		cout << "Failed creating socket error num %d" << WSAGetLastError();
		exit(0);
	}
	else
	{
		return s;
	}
}




//initiate a server
sockaddr_in Guy_Socket::initServer(int port)
{
	sockaddr_in client;
	client.sin_family = AF_INET;
	client.sin_addr.s_addr = INADDR_ANY;
	client.sin_port = htons(port);
	return client;
}





//bind a socket to the listen socket
void Guy_Socket::bind_sock(SOCKET listenSocket, const struct sockaddr *name, int namelen)
{
	if (bind(listenSocket, name, namelen) == SOCKET_ERROR)
	{
		cout << "Error binding ! code: %d\n" << WSAGetLastError();
	}
}




//start to listen to a socket
void Guy_Socket::listening(SOCKET s)
{
	if (listen(s, SOMAXCONN) == SOCKET_ERROR)
	{
		cout << "listen function failed with error: %d\n" << WSAGetLastError();
	}
}
//recive data from the client



void Guy_Socket::recive(SOCKET acceptSocket, char *str, int len, int flag)
{
	if (recv(acceptSocket, str, len, flag) == SOCKET_ERROR)
	{
		cout << "Error reciving! code: %d" << WSAGetLastError();
		closesocket(acceptSocket);
	}
}




//send data to the client
void Guy_Socket::send_data(SOCKET acceptSocket, const char *str, int len, int flag)
{
	if (send(acceptSocket, str, len, flag) == SOCKET_ERROR)
	{
		cout << "send failed with error: %d\n" << WSAGetLastError();
		closesocket(acceptSocket);
	}
}