#pragma once

#include <winsock2.h>
#include <windows.h>
#include <iostream>
#include <string>
using namespace std;

class Guy_Socket
{
public:
	Guy_Socket(int port,string data);
private:
	//initiate a socket
	SOCKET initSocket();
	//initiate a server
	sockaddr_in initServer(int port);
	//bind a socket to the listen socket
	void bind_sock(SOCKET listenSocket, const struct sockaddr *name, int namelen);
	//start to listen to a socket
	void listening(SOCKET s);
	//recive data from the client
	void recive(SOCKET acceptSocket, char *str, int len, int flag);
	//send data to the client
	void send_data(SOCKET acceptSocket, const char *str, int len, int flag);

};

